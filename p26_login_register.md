### Login & Register Laravel
- membuat project baru
```
$ composer create-project --prefer=dist laravel/laravel laravelku
```

- persiapan database
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravelku
DB_USERNAME=root
DB_PASSWORD=root
```

- ubah file migrasi 'create_user_table'
```
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
```

- menjalankan migrate
```
$ php artisan migrate
```

### membuat login dg mudah laravel
- jalankan auth
```
$ php artisan make:auth
```

- jalankan laravel
```
$ php artisan serve
```

- check route
```
$ php artisan route:list
```
