### Session Laravel
- membuat route
```
Route::get('/session/tampil','SessionController@tampil');
Route::get('/session/buat','SessionController@buat');
Route::get('/session/hapus','SessionController@hapus');
```

- membuat controller
```
$ php artisan make:controller SessionController
```

- ubah controller tsb
```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class TesController extends Controller
{
	// menampilkan isi session
	public function tampil(Request $request) {
		if($request->session()->has('islogin')){
			// get one
			// echo $request->session()->get('isaccess');

			// get all
			// echo json_encode($request->session()->all());

			// with library
			echo json_encode(Session::all());
		}else{
			echo 'Tidak ada data dalam session.';
		}
	}

	// membuat session
	public function buat(Request $request) {
		$data = array(
			'islogin' => true,
			'username' => 'moeamar',
			'isaccess' => 'author'
		);
		$request->session()->put($data);
		echo "Data telah ditambahkan ke session.";
	}

	// menghapus session
	public function hapus(Request $request) {
		// hapus satuan
		// $request->session()->forget('username');

		// hapus all session
		Session::flush();
		echo "Data telah dihapus dari session.";
	}
}
```

- jalankan laravel
```
$ php artisan serve
```