### Laravel Hash
- membuat route
```
Route::get('/security/hash', 'DikiController@hash')
```

- membuat controller
```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DikiController extends Controller
{
	public function hash(){
		$hash_password_saya = Hash::make('halo123');
		echo $hash_password_saya;
	}

}
```

- menjalankan serve
```
$ php artisan serve
```

### Memverifikasi hash laravel
- verifikasi data
```
if (Hash::check('password_yang_dimasukkan', $password_dari_db)) {
    // true
}else{
    // false
}
```

