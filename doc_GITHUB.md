# api

test api repo in gitlab

# Git Command
- inisialisasi projek
```
$ git init
```

- remote project
```
$ git remote add origin https://gitlab.com/moeclay/api.git
```

- pull project, (membuat repo lokal sama dg repo server)
```
$ git pull origin master
```

- add, commit, push
```
$ git add READMe.md
$ git commit -m "001-push-01"
$ git push -u origin master
```

- mencheck posisi branch
```
$ git status
```


- menampilkan list branch
```
$ git branch
```

- membuat branch baru
```
$ git checkout -b tester
```


- menghapus salah satu branch
```
$ git branch -d tester
```

- memilih branch
```
$ git checkout tester
$ git push -u origin tester
```

- mengembalikan ke versi sebelumnya
```
$ git log
$ git checkout hash_log nama_file.md
$ git add .
$ git commit -m "berhasil-dikembalikan"
$ git push -u origin master
```