### Started Laravel

- install laravel versi 5.8
```
# versi 5.8
$ composer create-project laravel/laravel NamaProjek "5.8.*" --prefer-dist

# versi terbaru
$ composer create-project laravel/laravel LatihanAnakIT --prefer-dist
```

- hapus cache config
```
$ php artisan config:cache
```

- environment file .env

- menjalankan migrate
```
$ php artisan migrate 
```

- sekarang laravel mendukung fitur login
- mulai gunakan api/user endpoint
- cek list route
```
$ php artisan route:list
```

- menjalankan laravel
```
$ php artisan serve --host=127.0.0.1
```