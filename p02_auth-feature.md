### auth laravel login/register

- gunakan fitur scaffolding auth
$ php artisan make:auth


- sekarang laravel mendukung fitur login
- mulai gunakan api/user endpoint


- cek list route
```
$ php artisan route:list
```


```
- GET
- http://localhost:8000/api/user
- Headers
Accept: application/json
Authorization: Bearer xxx
```

- error api_token
- alter tables users dg migrate
```
$ php artisan make:migration add_api_token_on_users --tables=users
```

- add is_disabled
```
$ php artisan make:migration add_is_disabled_on_users --tables=users
```

- menjalankan migrate
```
$ php artisan migrate
```


- cancel migration manual
- delete file
```
$ php artisan migrate:fresh
```

- mundur 1 langkah migrate
```
$ php artisan migrate:rollback
```

- edit database/migrations/file_migrate.php
- ubah di up() & down()
```
public function up()
{
    Schema::table('users', function (Blueprint $table) {
        $table->string('api_token', 100)->unique()->after('remember_token');
    });
}

public function down()
{
    Schema::table('users', function (Blueprint $table) {
        $table->dropColumn('api_token');
    });
}
```

- jalankan migrate untuk alter table
```
$ php artisan migrate
```

- tes di postman 
- error: "Unauthenticated"


- buat sample user seeder
```
$ php artisan make:seeder UserSeeder
```

- edit file database/seeds/UserSeeder.php
```
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

DB::table('users')->insert([
	'name' => 'Tester',
    'email' => 'tester@domain.com',
    'password' => Hash::make('123456'),
    'api_token' => Str::random(60),
]);
```

- jalankan seeder
```
$ php artisan db:seed --class=UserSeeder
```

- lakukan percobaan kembali di postman
```
Accept: application/json
Authorization: Bearer dfafs3rdfewrtwrcr234qdaf4
```

- hidden api_token response
- edit file app/User.php
```
# hidden data
protected $hidden = [
    ‘password’, ‘remember_token’,’api_token’
];

# fillable to insert register
protected $fillable = [
    'name', 'email', 'password','api_token',
];
```

- edit file app/Http/Controllers/Auth/RegisterController.php
```
protected function create(array $data)
{
    return User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => bcrypt($data['password']),
        'api_token' => Str::random(60),
    ]);
}
```
