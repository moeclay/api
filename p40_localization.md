### Laravel Localization
- menyiapkan library semacam kamus di folder 'resources/lang'

- buat folder
```
- /resources/lang/en
- /resources/lang/en
```

- buat resources/lang/en/biodata.php
```
<?php
return [
	'welcome' => 'Welcome to our website.',
	"title" => "Please fill form below correctly.",
	"profil" => [
		"name" => "Your Name",
		"address" => "Your Address",
		"hobby" => "Your Hobby",
		"job" => "Your Job",
	],
	"button" => "Save",
	"thank" => "Thank you for your contribution.",
];
```

- buat resources/lang/id/biodata.php
```
<?php
return [
	'welcome' => 'Selamat datang di website kami.',
	"title" => "Silahkan isi formulir berikut dengan benar.",
	"profil" => [
		"name" => "Nama Lengkap Anda",
		"address" => "Alamat Anda",
		"hobby" => "Hobi Anda",
		"job" => "Pekerjaan Anda",
	],
	"button" => "Simpan",
	"thank" => "Terima kasih atas kontribusi anda.",
];
```

- cara pemanggilan
```
//cara pertama
@lang("nama_file_bahasa/key_array_nya")

//cara kedua
{{ __("nama_file_bahasa/key_array_nya") }}
```

- buat route
```
// localization default
Route::get('/form', function () {
    return view('bahasa/biodata');
});
```

- buat view
```
<!DOCTYPE html>
<html>
<head>
	<title>Tutorial Multi Bahasa Localization Laravel - www.malasngoding.com</title>
</head>
<body>

	<h2>@lang('biodata.welcome')</h2>

	<p>@lang('biodata.title')</p>

	<form>
		<div>
			<label>@lang('biodata.profil.name')</label>
			<input type="text" name="">
		</div>

		<div>
			<label>@lang('biodata.profil.address')</label>
			<input type="text" name="">
		</div>

		<div>
			<label>@lang('biodata.profil.hobby')</label>
			<input type="text" name="">
		</div>

		<div>
			<label>@lang('biodata.profil.job')</label>
			<input type="text" name="">
		</div>

		<div>
			<button>@lang('biodata.button')</button>
		</div>
	</form>

	<p>@lang('biodata.thank')</p>

</body>
</html>
```

- ubah file 'app/config.php'
```
- locale: 'en' 
- locale: 'id' 

```

- localization dg url
```
// localization pilih bahasa
Route::get('/{locale}/form', function ($locale) {
    App::setLocale($locale);
    return view('bahasa/biodata');
});
```
