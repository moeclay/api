### Action URL Laarvel
- buat route
```
Route::get('halo/{nama}', 'HaloController@halo');
Route::get('halo', 'HaloController@panggil');
```

- buat controller
```
$ php artisan make:controller HalloController
```

- ubah controller
```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HalloController extends Controller
{
    public function halo($nama)
    {
    	return "Halo, " . $nama;
    }

    public function panggil()
    {
    	// call action route
    	return action('HaloController@halo', ['nama' => 'Diki']);
    }
}
```

- panggil
```
http://localhost:8000/halo/reza
http://localhost:8000/halo
```
