### Encrypt & Decrypt
- membuat controller
```
$ php artisan make:controller DikiController
```

- membuat route
```
Route::get('/security/enc', 'DikiController@index')
```

- ubah diki controller
```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class DikiController extends Controller
{
	public function enkripsi(){
		$encrypted = Crypt::encryptString('Belajar Laravel Di malasngoding.com');
		$decrypted = Crypt::decryptString($encrypted);

		echo "Hasil Enkripsi : " . $encrypted;
		echo "<br/>";
		echo "<br/>";
		echo "Hasil Dekripsi : " . $decrypted;
	}
}
```

### Membuat Enkirpsi URL diLaravel
- membuat route
```
Route::get('/security/data/', 'DikiController@data');
Route::get('/security/data/{data_rahasia}', 'DikiController@data_proses');
```

- ubah controller
```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class DikiController extends Controller
{
	public function data(){
		
		$parameter =[
			'nama' => 'Diki Alfarabi Hadi',
			'pekerjaan' => 'Programmer',
		];
		$enkripsi= Crypt::encrypt($parameter);
		echo "<a href='/data/".$enkripsi."'>Klik</a>";
	}

	public function data_proses($data){
		$data = Crypt::decrypt($data);

		echo "Nama : " . $data['nama'];
		echo "<br/>";
		echo "Pekerjaan : " . $data['pekerjaan'];
	}
}
```
